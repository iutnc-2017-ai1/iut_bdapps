<?php


namespace gamepedia\views; 

class ViewCompagnie{
	private $content;
	private $listeCompagny;
	
	public function __construct($content){
		$this->listeCompagny = $content;
	}
	
	private function afficherCompagny(){
		foreach($this->listeCompagny as $val){
			echo("$val <br>");
		}
		
	}
	
	private function afficherSony(){
		foreach($this->listeCompagny as $val){
			echo ("$val <br>");
		}
	}
	
	public function render($methode){
		switch($methode){
			case 1:
				$this->afficherCompagny();
				break;
			case 2:
				$this->afficherSony();
				break;
		}
	}
}