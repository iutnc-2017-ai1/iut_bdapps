<?php
namespace gamepedia\models;

class Compagnie extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'company';
	protected $primaryKey = 'id';
	public $timestamps = false;
	
	public function game(){
		return $this->belongsToMany("\gamepedia\models\Game","game_publishers","comp_id","game_id")->select('name');
	}
}