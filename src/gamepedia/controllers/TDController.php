<?php
namespace gamepedia\controllers;

use gamepedia\views\ViewPage;
use gamepedia\views\ViewTD;

class TDController extends AbstractController
{
    public function td1() {
        $content = new ViewTD();
        $content = $content->td1();
        $page = new ViewPage("TD1",$content);
        $page->renderPage();
    }

    public function td2() {
        $content = new ViewTD();
        $content = $content->td2();
        $page = new ViewPage("TD2",$content);
        $page->renderPage();
    }

    public function td3() {
        $content = new ViewTD();
        $content = $content->td3();
        $page = new ViewPage("TD3",$content);
        $page->renderPage();
    }

    public function td4() {
        $content = new ViewTD();
        $content = $content->td4();
        $page = new ViewPage("TD4",$content);
        $page->renderPage();
    }

    public function td56() {
        $content = new ViewTD();
        $content = $content->td56();
        $page = new ViewPage("TD5&6",$content);
        $page->renderPage();
    }
}