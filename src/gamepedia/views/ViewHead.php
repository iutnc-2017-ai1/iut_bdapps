<?php

namespace gamepedia\views;

class ViewHead
{
    protected $name;
    protected $content;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function head(){
        $urlStyleCss = URL_CSS."style.css";
        $urlBootstrapCss = URL_CSS."bootstrap.min.css";
        $script = URL_JS."actions.js";
        $head = <<<TAG
<head>
            <title>$this->name</title>
            <link rel="stylesheet" href="$urlBootstrapCss">
            <link rel="stylesheet" href="$urlStyleCss">
            <script type="text/javascript" src="$script"></script>
            <link href="http://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
            <meta http-equiv="Site-Enter" content="revealTrans(Duration=2.0,Transition=1)">
            <meta name="viewport" content="width=375px, initial-scale=1.0, shrink-to-fit=no, user-scalable=0">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        </head>
TAG;
        return $head;
    }
}