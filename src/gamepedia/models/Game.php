<?php
namespace gamepedia\models;

class Game extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'game';
	protected $primaryKey = 'id';
	public $timestamps = true;

	public function charactersDecks(){
		return $this->belongsToMany("\gamepedia\models\Character","game2character","game_id","character_id")->select('name','deck');
	}

	public function characters() {
		return $this->belongsToMany('\gamepedia\models\Character', 'game2character', 'game_id', 'character_id');
	}

	public function checkRating3plus() {
		return $this->belongsToMany('\gamepedia\models\Rating', 'game2rating', 'game_id', 'rating_id')->where('name', 'like', '%3+%');
	}

	public function companyInc() {
		return $this->belongsToMany('\gamepedia\models\Compagnie', 'game_publishers', 'game_id', 'comp_id')->where('name', 'like', '%Inc.%');
	}

	public function cero() {

	}

	public function plateforms() {
		return $this->belongsToMany('\gamepedia\models\Plateforme', 'game2platform', 'game_id', 'platform_id')->select('id', 'name', 'alias', 'abbreviation');
	}
}