<?php
namespace gamepedia\controllers;

use gamepedia\models\Game;
use gamepedia\models\Commentaire;
use gamepedia\models\Plateforme;

class APIController extends AbstractController
{
	public function game_id($id) {

		$this->app->response()->header('Content-Type', 'application/json');
		$game = Game::select('id', 'name', 'alias', 'deck', 'description', 'original_release_date')->where('id', $id)->with('plateforms')->first();
		if ($game != null) {
			$ret = array(
				'game' => array(
					'id' => $game->id,
					'name' => $game->name,
					'alias' => $game->alias,
					'deck' => $game->deck,
					'description' => $game->description,
					'original_release_date' => $game->original_release_date
				),
				'links' => array(
					'comments' => array('href' => $this->app->urlFor('game_comments', ['id' => $game->id])),
					'characters' => array('href' => $this->app->urlFor('game_characters', ['id' => $game->id])),
				),
				'plateforms' => array()
			);
			foreach ($game->plateforms as $g) {
				$ret['plateforms'][] = array(
					'plateform' => array(
						'id' => $g->id,
						'name' => $g->name,
						'alias' => $g->alias,
						'abbreviation' => $g->abbreviation
					),
					'links' => array('self' => array('href' => $this->app->urlFor('game_plateform', ['id' => $g->id])))
				);
			}
			echo json_encode($ret);
		} else {
			echo json_encode(array(
				'success' => false,
				'message' => 'Id introuvable'
			));
		}

	}

	public function games() {
		$page = 0;
		if (isset($_GET['page'])) {
			$page = $_GET['page'];
			if ($page < 0) {
				$page = 0;
			}
		}
		$this->app->response()->header('Content-Type', 'application/json');
		$itemsCount = 200;
		$games = Game::select('id', 'name', 'alias', 'deck')->skip($page * $itemsCount)->take($itemsCount)->get();
		if ($games != null) {
			$ret = array('games'=>array());
			foreach ($games as $g) {
				$ret['games'][] = array(
					'game' => array(
						'id' => $g->id,
						'name' => $g->name,
						'alias' => $g->alias,
						'deck' => $g->deck
					),
					'links' => array('self' => array('href' => $this->app->urlFor('game_id', ['id' => $g->id])))
				);
			}
			$ret['links'] = array(
				'prev' => array('href' => '/api/games?page='.($page > 0 ? $page - 1 : 0)),
				'next' => array('href' => '/api/games?page='.($page + 1))
			);
			echo json_encode($ret);
		}
	}

	public function game_comments($id) {
		$this->app->response()->header('Content-Type', 'application/json');
		$comments = Commentaire::select('id', 'titre', 'contenu', 'dateCrea', 'fk_user')->where('fk_jeu', $id)->take(100)->get();
		if ($comments != null) {
			if ($comments->count() > 0) {
				echo $comments->toJSON();
			} else {
				echo json_encode(array(
					'message' => 'No comments available'
				));
			}
		} else {
			echo json_encode(array(
				'success' => false,
				'message' => 'Id introuvable'
			));
		}
	}
}