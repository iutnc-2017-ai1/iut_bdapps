<?php
/**
 * Created by PhpStorm.
 * User: thoma
 * Date: 06/03/2017
 * Time: 09:54
 */

namespace gamepedia\models;

class RatingBoard extends \Illuminate\Database\Eloquent\Model{
    protected $table = 'rating_board';
    protected $primaryKey = 'id';
    public $timestamps = true;
}