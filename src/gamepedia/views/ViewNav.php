<?php

namespace gamepedia\views;

class ViewNav
{
    protected $user;

    public function __construct($user=NULL)
    {
        $this->user = $user;
    }

    public function nav(){
        $app=\Slim\Slim::getInstance();
        $script = URL_JS."actions.js";
		$linkHome = $app->urlFor('home');
        $linktd1 = $app->urlFor('td1');
        $linktd2 = $app->urlFor('td2');
        $linktd3 = $app->urlFor('td3');
        $linktd4 = $app->urlFor('td4');
        $linktd5 = $app->urlFor('td56');
        $urlScript = URL_JS."actions.js";
        $nav = <<<TAG
<nav class="elementColorBase">
            <div class="container">
                <ul id="navBar" class="nav_bar responsive_active">
                    <li><a class="elementFontBase" href="$linkHome">Gamepedia</a></li><li>
                    <a class="elementFontBase" href="$linktd1">TD1</a></li><li>
                    <a class="elementFontBase" href="$linktd2">TD2</a></li><li>
                    <a class="elementFontBase" href="$linktd4">TD4</a></li><li>
                    <a class="elementFontBase" href="$linktd5">TD5&6</a></li><li>
                    <a class="elementFontBase fontSizeUp"><span id="arrow">III</span></a></li>
                </ul>
            </div>
        </nav>
        <script src="$urlScript"></script>
TAG;
            return $nav;
    }
}