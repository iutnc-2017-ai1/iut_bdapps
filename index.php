<?php

require_once  'vendor/autoload.php';
session_start();

define('URL_ROOT', substr($_SERVER['SCRIPT_NAME'], 0, -9));
define('URL_CSS', URL_ROOT.'web/css/');
define('URL_JS', URL_ROOT.'web/javascript/');
define('URL_IMAGES', URL_ROOT.'web/images/');

use Illuminate\Database\Capsule\Manager as DB;
use gamepedia\controllers\Router;

$db = new DB();
$db->addConnection(parse_ini_file('src/conf/db.config.ini'));
$db->setAsGlobal();
$db->bootEloquent();

DB::connection()->enableQueryLog();
//DB::enableQueryLog();
//DB::getQueryLog();

/*
 * Initialisation de Slim
 */
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
		'enableQueryLog' => true
    ],
    'templates.path' => 'src/gamepedia/views/templates'
];

$app = new \Slim\Slim($configuration);
//Routeur
$router = new Router($app);

//Création des routes
$router->get('/', 'Home@home')->name('home');
$router->get('/td1/', 'TD@td1')->name('td1');
$router->get('/td2/', 'TD@td2')->name('td2');
$router->get('/td3/', 'TD@td3')->name('td3');
$router->get('/td4/', 'TD@td4')->name('td4');
$router->get('/td56/', 'TD@td56')->name('td56');


/*
 * TD1
 */
$router->get('/td1/games(/:name)(/)', 'Game@liste')->name('q1');
$router->get('/td1/distants(/)', 'Game@distants')->name('q2');
$router->get('/td1/base(/)', 'Plateform@base')->name('q3');
$router->get('/td1/compagnyJapon','Compagnie@afficherJapon')->name('q4');
$router->get('/td1(/:page)(/)', 'Game@page')->name('q5');
/*
 * TD2
 */
$router->get('/td2/q1','Game@perso')->name('q1td2');
$router->get('/td2/q2', 'Game@persos')->name('q2td2');
$router->get('/td2/q3','Compagnie@afficherSony')->name('q3td2');
$router->get('/td2/q4(/:name)(/)', 'Game@rating')->name('q4td2');
$router->get('/td2/q5','Game@plusDe3')->name('q5td2');
$router->get('/td2/q6','Game@rating3')->name('q6td2');
$router->get('/td2/q7','Game@q7')->name('q7td2');
$router->get('/td2/q8','Game@q8')->name('q8td2');
$router->get('/td2/q9(/:name)(/)','Genre@q9')->name('q9td2');
$router->get('/td4/q1','TD4@q1')->name('q1td4');
$router->get('/td4/q2','TD4@q2')->name('q2td4');

/*
 * TD5-6
 */
$router->get('/api/games/:id(/)', 'API@game_id')->name('game_id');
$router->get('/api/games(/)', 'API@games')->name('games');
$router->get('/api/games/:id/comments(/)', 'API@games_comments')->name('game_comments');
$router->get('/api/games/:id/characters(/)', 'API@game_characters')->name('game_characters');
$router->get('/api/plateforms/:id(/)', 'API@game_plateform')->name('game_plateform');


$app->run();

/*foreach (DB::getQueryLog() as $q) {
	echo '~~~~~~~~~~~~~~~~~~<br/>';
	echo 'query: '.$q['query'].'<br/>';
	echo '~~~ bindings : [<br/>';
	foreach ($q['bindings'] as $b) {
		echo ' '.print_r($b, true).', ';
	}
	echo ']<br/>';
}*/