<?php
namespace gamepedia\views;

class ViewTD
{
    public function td1(){
        $app=\Slim\Slim::getInstance();
        $linkq1 = $app->urlFor('q1');
        $linkq2 = $app->urlFor('q2');
        $linkq3 = $app->urlFor('q3');
        $linkq4 = $app->urlFor('q4');
        $linkq5 = $app->urlFor('q5', ['page'=>1]);
        $content = <<<TAG
    <div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Liens du TD1</h5>
                </div>
                <div class='articleCorps'>
               
                <ul class="listeLien">
                    <li><a href="$linkq1">Question 1</a></li>
                    <li><a href="$linkq2">Question 2</a></li>
                    <li><a href="$linkq3">Question 3</a></li>
                    <li><a href="$linkq4">Question 4</a></li>
                    <li><a href="$linkq5">Question 5</a></li>
                </ul>
               
                </div>
            </div>
        </div>
    </div>
TAG;
        return $content;
    }

    public function td2(){
        $app=\Slim\Slim::getInstance();
        $linkq1 = $app->urlFor('q1td2');
        $linkq2 = $app->urlFor('q2td2');
        $linkq3 = $app->urlFor('q3td2');
        $linkq4 = $app->urlFor('q4td2');
        $linkq5 = $app->urlFor('q5td2');
        $linkq6 = $app->urlFor('q6td2');
        $linkq7 = $app->urlFor('q7td2');
        $linkq8 = $app->urlFor('q8td2');
        $linkq9 = $app->urlFor('q9td2');
        $content = <<<TAG
    <div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Liens du TD2</h5>
                </div>
                <div class='articleCorps'>
               
                <ul class="listeLien">
                    <li><a href="$linkq1">Question 1</a></li>
                    <li><a href="$linkq2">Question 2</a></li>
                    <li><a href="$linkq3">Question 3</a></li>
                    <li><a href="$linkq4">Question 4</a></li>
                    <li><a href="$linkq5">Question 5</a></li>
                    <li><a href="$linkq6">Question 6</a></li>
                    <li><a href="$linkq7">Question 7</a></li>
                    <li><a href="$linkq8">Question 8</a></li>
                    <li><a href="$linkq9">Question 9</a></li>
                </ul>
               
                </div>
            </div>
        </div>
    </div>
TAG;
        return $content;
    } public function td3(){
    $app=\Slim\Slim::getInstance();
    //$linkq1 = $app->urlFor('q1td3');

    $content = <<<TAG
    <div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Liens du TD3</h5>
                </div>
                <div class='articleCorps'>
               
                <ul class="listeLien">
                    <li><a href="">Question 1</a></li>
                </ul>
               
                </div>
            </div>
        </div>
    </div>
TAG;
    return $content;
}
	
	
	public function td4() {
        $app=\Slim\Slim::getInstance();
        $linkq1 = $app->urlFor('q1td4');
        $linkq2 = $app->urlFor('q2td4');
        $content = <<<TAG
    <div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Liens du TD4</h5>
                </div>
                <div class='articleCorps'>
               
                <ul class="listeLien">
                    <li><a href="$linkq1">Question 1</a></li>
                    <li><a href="$linkq2">Question 2</a></li>
                </ul>
               
                </div>
            </div>
        </div>
    </div>
TAG;
        return $content;
    }

    public function td56() {
        $app=\Slim\Slim::getInstance();
        $content = <<<TAG
    <div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Liens du TD4</h5>
                </div>
                <div class='articleCorps'>
               
                <ul class="listeLien">
                    <li>Disponible dans la classe APIController</li> 
                </ul>
               
                </div>
            </div>
        </div>
    </div>
TAG;
        return $content;
    }


}