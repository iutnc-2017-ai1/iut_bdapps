<?php
namespace gamepedia\controllers;

use gamepedia\models\Genre;
use gamepedia\views\ViewPage;

class GenreController extends AbstractController
{
	public function q9($name='Unknown') {
		$g = new Genre;
		$g->name = $name;
		$g->save();
		$g->assoc()->attach([12, 56, 345]);
        $content = "<div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Résultat de la question 9</h5>
                </div>
                <div class='articleCorps'>
               
                Genre ajouté : $name
               
                </div>
            </div>
        </div>
    </div>";
        $page = new ViewPage("Games", $content);
        $page->renderPage();
	}
}