<?php
namespace gamepedia\views;

class ViewHome
{
    public function home(){
        $content = <<<TAG
    <div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Bienvenue sur gamepedia</h5>
                </div>
                <div class='articleCorps'>
               
                <ul class="listeNom">
                    <li>Florian Hervieux</li>
                    <li>Thomas Ferary</li>
                    <li>Hugo Benier</li>
                    <li>Hugo Marx</li>
                </ul>
               
                </div>
            </div>
        </div>
    </div>
TAG;
    return $content;
    }
}