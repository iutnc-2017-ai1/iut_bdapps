<?php
namespace gamepedia\models;

class Plateforme extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'platform';
	protected $primaryKey = 'id';
	public $timestamps = true;
}