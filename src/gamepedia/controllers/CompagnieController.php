<?php

namespace gamepedia\controllers;

use gamepedia\views\ViewHome;
use gamepedia\views\ViewPage;
use gamepedia\views\ViewCompagnie;
use Slim\Slim;
use gamepedia\models\Compagnie;

class CompagnieController{
	
	public static function afficherJapon(){
		$listj = Compagnie::select('name')->where('location_country','=','Japan')->get();
        $content = 'Results count: '.count($listj).'<br/><br/>';
        foreach ($listj as $g) {
            $content .= $g->name.'<br/>';
        }
        $content = "<div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Résultat de la question 4</h5>
                </div>
                <div class='articleCorps'>
               
                $content
               
                </div>
            </div>
        </div>
    </div>";
        $vue = new ViewPage("afficherJapon",$content);
        $vue->renderPage();
	}
	
	public function afficherSony(){
		$tab=[];
		$comp=Compagnie::where('name','like','%Sony%')->get();
        $content = 'Results count: '.count($comp).'<br/><br/>';
		foreach($comp as $values){
			foreach($values->game as $c){
                $content .= $c->name.'<br/>';
			}
		}
        $content = "<div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Résultat de la question 3</h5>
                </div>
                <div class='articleCorps'>
               
                $content
               
                </div>
            </div>
        </div>
    </div>";
		$vue = new ViewPage("afficherSony",$content);
        $vue->renderPage();
	}
}