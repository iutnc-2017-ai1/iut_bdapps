<?php

namespace gamepedia\views;

class ViewGame{
	private $content;
	private $listeGame;
	
	public function __construct($content) {
		$this->listeGame = $content;
	}
	
	private function afficherPerso() {
		foreach($this->listeGame as $val) {
			echo("$val <br/>");
		}
	}
	
	private function afficherPersos() {
		foreach($this->listeGame as $val) {
			echo $val['name'].'<br/>';
		}
	}
	
	public function render($methode) {
		switch($methode){
			case 1:
				$this->afficherPerso();
				break;
			case 2:
				$this->afficherPersos();
				break;
		}
	}
}