<?php
namespace gamepedia\controllers;

use gamepedia\models\User;
use gamepedia\models\Commentaire;

class TD4Controller extends AbstractController
{
	public function q1() {
		$faker = \Faker\Factory::create();
		
		for ($i = 0; $i < 2; $i++) {
			$u = new User;
			$u->nom = $faker->firstName;
			$u->prenom = $faker->name;
			$u->email = $faker->email;
			$u->tel = $faker->phoneNumber;
			$u->dateNaiss = new \DateTime($faker->dateTimeThisCentury->format('Y-m-d'));
			$u->save();
			for ($j = 0; $j < 3; $j++) {
                $c = new Commentaire;
                $c->titre = $faker->text(20);
                $c->contenu = $faker->text(200);
                $c->dateCrea = new \DateTime($faker->dateTimeThisCentury->format('Y-m-d'));
                $c->fk_user = $u->email;
                $c->fk_jeu = 12342;
                $c->save();
            }
		}
	}
	
	public function q2() {
		set_time_limit(0);
		$faker = \Faker\Factory::create();
		for ($i = 0; $i < 25000; $i++) {
			$u = new User;
			$u->nom = $faker->firstName;
			$u->prenom = $faker->name;
			$u->email = $faker->email;
			$u->tel = $faker->phoneNumber;
			$u->dateNaiss = new \DateTime($faker->dateTimeThisCentury->format('Y-m-d'));
			$u->save();
			for ($j = 0; $j < 10; $j++) {
				$c = new Commentaire;
				$c->titre = $faker->text(20);
				$c->contenu = $faker->text(200);
				$c->dateCrea = new \DateTime($faker->dateTimeThisCentury->format('Y-m-d'));
				$c->fk_user = $u->email;
				$c->fk_jeu = 12342;
				$c->save();
			}
		}
		set_time_limit(120);
	}
}