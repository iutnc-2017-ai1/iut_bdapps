<?php

namespace gamepedia\views;

class ViewPage
{
    private $head;
    private $nav;
    private $content;

    public function __construct($name, $content)
    {
        $this->head = new ViewHead($name);
        $this->head = $this->head->head();
        $this->nav = new ViewNav();
        $this->nav = $this->nav->nav();
        $this->content = $content;
    }

    public function renderPage(){
        $page = <<<TAG
        <!DOCTYPE html>
        <html>
        $this->head
        <body>
        $this->nav
        $this->content
        <script src="%sjquery-3.1.1.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="%spl.js"></script>
        </body>
        </html>
TAG;
        $page = sprintf($page, URL_JS, URL_JS);
    echo $page;
    }
}