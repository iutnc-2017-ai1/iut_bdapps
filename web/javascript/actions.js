/**
 * Created by thomasferary on 05/02/2017.
 */


var page = {
    modules : {}
};

page.modules.app=(function (){
    var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
    var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;
    var pointer = document.querySelector("#pointer");
    var navBar = document.querySelector("#navBar");
    var click = 0;

    return{

        start: function () {
            window.addEventListener('load', function (e) {
                pointer.addEventListener('click', function (e) {
                    click += 1;
                    if(click === 1){
                        navBar.classList.add("animMenuActive");
                    }else{
                        if(navBar.classList.contains("animMenuActive")){
                            navBar.classList.toggle("animMenuInactive");

                        }else if(navBar.classList.contains("animMenuInactive")){
                            navBar.classList.toggle("animMenuInactive");
                        }
                    }
                });
            });
        }
    }
}());

page.modules.app.start();
