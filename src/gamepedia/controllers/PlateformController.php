<?php
namespace gamepedia\controllers;

use gamepedia\models\Game;
use gamepedia\views\ViewHome;
use gamepedia\views\ViewPage;
use gamepedia\models\Plateforme;

class PlateformController extends AbstractController
{
	public function base() {
		$games = Plateforme::where('install_base', '>=', '10000000')->get();
		$content = 'Results count: '.count($games).'<br/><br/>';
		foreach ($games as $g) {
			$content .= $g->name.'<br/>';
		}
        $content = "<div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Résultat de la question 3</h5>
                </div>
                <div class='articleCorps'>
               
                $content
               
                </div>
            </div>
        </div>
    </div>";
		$page = new ViewPage("intallBase", $content);
		$page->renderPage();
	}

}