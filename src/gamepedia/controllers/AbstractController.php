<?php
namespace gamepedia\controllers;

abstract class AbstractController
{
	protected $app;
	
	function __construct(\Slim\Slim $app = null) {
		if (empty($app)) {
			$app = \Slim\Slim::getInstance();
		}
		$this->app = $app;
	}
	
	/**
	 * Calculate a precise time difference.
	 * @param string $start result of microtime()
	 * @param string $end result of microtime(); if NULL/FALSE/0/'' then it's now
	 * @return flat difference in mili-seconds, calculated with minimum precision loss
	 */
	function microtime_diff($start, $end = null) {
		if (!$end) {
			$end = microtime();
		}
		list($start_usec, $start_sec) = explode(" ", $start);
		list($end_usec, $end_sec) = explode(" ", $end);
		$diff_sec = intval($end_sec) - intval($start_sec);
		$diff_usec = floatval($end_usec) - floatval($start_usec);
		return ((floatval($diff_sec) + $diff_usec) * 1000).'ms';
	}

	function getUserIP() {
		$client  = @$_SERVER['HTTP_CLIENT_IP'];
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
		$remote  = $_SERVER['REMOTE_ADDR'];
		if(filter_var($client, FILTER_VALIDATE_IP)) {
			$ip = $client;
		} elseif(filter_var($forward, FILTER_VALIDATE_IP)) {
			$ip = $forward;
		} else {
			$ip = $remote;
		}
		return $ip;
	}
    
    function isConnected(){
        return isset($_SESSION['id']); 
    }
	
	/**
	 * $msg (String) : the reason to go back (Unused)
	 */
	function goBack($msg='') {
		if (isset($_SERVER['HTTP_REFERER'])) {
			$this->app->response->redirect($_SERVER['HTTP_REFERER'], 303);
		} else {
			$this->app->response->redirect($this->app->urlFor('home'), 303);
		}
	}
}