<?php
namespace gamepedia\controllers;

use gamepedia\views\ViewHome;
use gamepedia\views\ViewPage;

class HomeController extends AbstractController
{
    public function home() {
        $content = new ViewHome();
        $content = $content->home();
        $page = new ViewPage("Home",$content);
        $page->renderPage();
    }
}