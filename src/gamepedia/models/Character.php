<?php
namespace gamepedia\models;

class Character extends \Illuminate\Database\Eloquent\Model{
	protected $table = 'character';
	protected $primaryKey = 'id';
	public $timestamps = true;
	
	public function game(){
		return $this->belongsToMany("\gamepedia\models\Game","game2character","character_id","game_id");
	}
	
}