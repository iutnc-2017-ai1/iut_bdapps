<?php
namespace gamepedia\controllers;

use gamepedia\models\Game;
use gamepedia\views\ViewHome;
use gamepedia\views\ViewPage;
use gamepedia\views\ViewGame;
use Illuminate\Database\Capsule\Manager as DB;

class GameController extends AbstractController
{
	public function liste($name='Mario') {
		$tb = microtime();
		$games = Game::where('name', 'LIKE', '%'.$name.'%')->get();
		$content = 'Results count: '.count($games).'<br/><br/>';
		foreach ($games as $g) {
			$content .= $g->name.'<br/>';
		}
		var_dump($this->microtime_diff($tb));
		$content = "<div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Résultat de la question 1</h5>
                </div>
                <div class='articleCorps'>
               
                $content
               
                </div>
            </div>
        </div>
    </div>";
        $page = new ViewPage("Games", $content);
        $page->renderPage();
    }
	
	public function distants() {
		$games = Game::take(442)->skip(21173)->get();
		$content = 'Results count: '.count($games).'<br/><br/>';
		foreach ($games as $g) {
			$content .= $g->name.'<br/>';
		}
        $content = "<div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Résultat de la question 2</h5>
                </div>
                <div class='articleCorps'>
               
                $content
               
                </div>
            </div>
        </div>
    </div>";
        $page = new ViewPage("Games", $content);
        $page->renderPage();
	}

    public function page($page=1) {
        $pagination = '';
		$page -= 1;
		
		$tb = microtime();
		$count = ceil(Game::count() / 500);
		$games = Game::take(500)->skip(500*$page)->get();
		var_dump($this->microtime_diff($tb));
		
        $content = 'Results count: '.count($games).'<br/><br/>';

        foreach ($games as $g) {
            $content .= $g->name.'<br/>';
        }
		
		for($i = 1; $i <= $count; $i++){
            $url = $this->app->urlFor('q5',['page' => $i]);
            $pagination .= "<a class=\"elementColorBase paginate\" href=\"$url\">$i</a>";
        }
        $content .= $pagination;

        $page = new ViewPage("GamesPage", $content);
        $page->renderPage();
    }

    public function rating($name='Mario'){
        $games = Game::where('name', 'LIKE', '%'.$name.'%')->get();
        $content = "";
        $page = new ViewPage("Games", $content);
        $page->renderPage();
    }
	public function perso(){
		$tab=[];
		$games = Game::where('id','=',12342)->get();
		foreach($games as $values){
			foreach($values->charactersDecks as $c){
				$tab[]=$c;
			}
		}
        $content = 'Results count: '.count($tab).'<br/><br/>';
        foreach ($tab as $g) {
            $content .= $g->name.' / Deck : '.$g->deck.'<br/>';
        }
        $content = "<div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Résultat de la question 1</h5>
                </div>
                <div class='articleCorps'>
               
                $content
               
                </div>
            </div>
        </div>
    </div>";
        $vue = new ViewPage("question 1",$content);
        $vue->renderPage();
	}
	
	
	
	public function persos() {
		$tb = microtime();
		$gam = [];
		$games = Game::where('name', 'like', 'Mario%')->with('characters')->get();
        $content = 'Results count: '.count($games).'<br/><br/>';
		
		/*foreach ($games as $v) {
			//var_dump($v);
			$content .= $v['name'].'<br/>';
		}*/
		
		foreach($games as $values) {
            $content .= $values['name'].'<br/><br/>Personnages : <br/><br/>';
			foreach($values->characters as $c) {
                $content .= $c['name'].'<br/>';
			}
		}
		var_dump($this->microtime_diff($tb));

        $content = "<div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Résultat de la question 2</h5>
                </div>
                <div class='articleCorps'>
               
                $content
               
                </div>
            </div>
        </div>
    </div>";

        $vue = new ViewPage("question 2",$content);
        $vue->renderPage();
	}
	

	public function rating3() {
		$games = Game::where('name', 'like', 'Mario%')->with('checkRating3plus')->get();
		$content = 'Results count: '.count($games).'<br/><br/>';
		foreach ($games as $g) {
			$content .= $g->name.'<br/>';
		}
        $content = "<div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Résultat de la question 6</h5>
                </div>
                <div class='articleCorps'>
               
                $content
               
                </div>
            </div>
        </div>
    </div>";
		$page = new ViewPage("Games", $content);
        $page->renderPage();
	}
	
	public function q7() {
		$games = Game::where('name', 'like', 'Mario%')->with('companyInc', 'checkRating3plus')->get();
        $content = 'Results count: '.count($games).'<br/><br/>';
		foreach ($games as $g) {
			$content .= $g->name.'<br/>';
		}
        $content = "<div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Résultat de la question 7</h5>
                </div>
                <div class='articleCorps'>
               
                $content
               
                </div>
            </div>
        </div>
    </div>";
		$page = new ViewPage("Games", $content);
        $page->renderPage();
	}

	public function plusDe3(){
		$res = DB::select("SELECT distinct name FROM `game` WHERE (select count(*) from  `game2character` NATURAL join `game` where name like 'Mario%' )>=3 and name like 'Mario%'");
        $content = 'Results count: '.count($res).'<br/><br/>';
		foreach($res as $r){
            $content .= $r['name'].'<br/>';
		}
        $content = "<div class='container page'>
        <div class='row'>
            <div class='col-md-12 article'>
                <div class='articleHead elementColorBase'>
                    <h5>Résultat de la question 5</h5>
                </div>
                <div class='articleCorps'>
               
                $content
               
                </div>
            </div>
        </div>
    </div>";
        $vue = new ViewPage("question 1",$content);
        $vue->renderPage();
	}
	public function q8() {
		$content = '';
		$page = new ViewPage("Games", $content);
        $page->renderPage();
	}
}